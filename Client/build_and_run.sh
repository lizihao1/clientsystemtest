#!/bin/bash
set -e

docker container prune -f
sleep 5s
docker image prune -f
sleep 5s 


# Start all the services defined in the
# demo-client/docker-compse.yaml file.
# The file refers to image that are locally or globally available.
# The image demo exist, because we have just build it.
# If the demo container is already running, and the image has changed,
# then docker-compose will create a new container from that image and
# replace the old container with the new container.
# Start docker-compose up with the -d option to run
# docker-compose in the background.
#docker-compose up -d

# Give Thorntail a chance to be ready.
#sleep 10s

mvn clean test
