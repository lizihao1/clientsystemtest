package behaviourtests;

import java.math.BigDecimal;

public class PaymentRequest {
	
	public String debtor;
	public String creditor;
	public BigDecimal amount;
	public String comment;
	
	public PaymentRequest() {};
	
	public PaymentRequest(String account1, String account2, BigDecimal i,String comment) {
		debtor = account1;
		creditor = account2;
		amount = i;
		this.comment = comment;
	}

}
