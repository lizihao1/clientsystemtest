package behaviourtests;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.Assert;

import io.cucumber.java.After;
import dtu.ws.fastmoney.*;
import userService.DtuPayUserRepresentation;
import tokenService.Token;
import DTUpay.DTUpay;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import behaviourtests.PaymentRequest;;
/**
 * 
 * @author Stuart, Bijan
 *
 */

public class PaymentSteps {
    
	private BankService bank = new BankServiceService().getBankServicePort();
	private List<String> accounts = new ArrayList<String>();
	private DTUpay dtuPay = new DTUpay(bank);
	private List<Token> tokens;
	private Token merchantToken;
	private Response response;
	private BigDecimal customerBalance;
	private BigDecimal merchantBalance;
	private int amount = 100;
	private String customerCprNumber;
	private String merchantCprNumber;
	private String customerAccount;
	private String merchantAccount;
	private User u;
	
    @Given("customer has bank account")
    public void customerHasBankAccount() throws BankServiceException_Exception {
		createUserAndSetCustomerInformation();
		createCustomerBankAccount(); // Remember created accounts for cleanup
		registerDtupayCustomer();
    }

    @Given("customer has account in DTUpay")
    public void customerHasAccountInDTUpay() {
    	tokens = dtuPay.requestTokens(customerCprNumber,1);
    	assertTrue(tokens.size() >= 1);
    }

    @Given("customer has at least one unused token")
    public void customerHasAtLeastOneUnusedToken() {
    	merchantToken = tokens.get(0);
    }

    @Given("merchant has bank account")
    public void merchantHasBankAccount() throws BankServiceException_Exception {
		createUserAndSetMerchantInformation();
		createMerchantBankAccount(); // remember created accounts for cleanup
		registerDtupayMerchant();
    }

    @When("merchant scans the token")
    public void merchantScansTheToken() {
    	merchantToken = tokens.get(0);
    }
    
    @When("requests payment for {int} kroner using the token")
	public void requestsPaymentForKronerUsingTheToken(int amt) throws Throwable {
		BigDecimal amount = new BigDecimal(amt);
		
		response = dtuPay.payWithToken(merchantToken,customerAccount,merchantAccount,amount);
	}

    @Then("the payment succeeds")
    public void thePaymentSucceeds() {
    	assertEquals(Response.Status.NO_CONTENT, response.getStatusInfo().toEnum());
//    	assertTrue(success);
    }

    @Then("money transfered from the customer account to merchant account in the bank")
    public void moneyTransferedFromTheCustomerAccountToMerchantAccountInTheBank() throws BankServiceException_Exception {
    	assertEquals(customerBalance.subtract(new BigDecimal(amount)),bank.getAccount(customerAccount).getBalance());
		assertEquals(merchantBalance.add(new BigDecimal(amount)),bank.getAccount(merchantAccount).getBalance());
    }

    @After
	public void cleanupUsedAccounts() throws BankServiceException_Exception {
		for (String a : accounts) {
			bank.retireAccount(a);
		}
	}
    
    /*
     * -----------------------------------------------------------------------------------------------------------------------------
     */
    
    private void registerDtupayCustomer() {
		dtuPay.registerCustomer(u.getCprNumber()
				,u.getFirstName()
				,u.getLastName()
				,customerAccount);
	}

	private void createCustomerBankAccount() throws BankServiceException_Exception {
		customerBalance = new BigDecimal(1000);
		customerAccount = bank.createAccountWithBalance(u, customerBalance);
		accounts.add(customerAccount);
	}

	private void createUserAndSetCustomerInformation() {
		u = new User();
		u.setCprNumber("280771110-0728");
		u.setFirstName("lee");
		u.setLastName("chuan");
	}
	
	private void registerDtupayMerchant() {
		dtuPay.registerMerchant(u.getCprNumber(),u.getFirstName(),u.getLastName(),merchantAccount);
	}
	
	private void createMerchantBankAccount() throws BankServiceException_Exception {
		merchantBalance = new BigDecimal(1000);
		merchantAccount = bank.createAccountWithBalance(u, merchantBalance);
		accounts.add(merchantAccount);
	}
	
	private void createUserAndSetMerchantInformation() {
		u = new User();
		u.setCprNumber("29111119185-1894");
		u.setFirstName("rui");
		u.setLastName("huang");
	}
}
