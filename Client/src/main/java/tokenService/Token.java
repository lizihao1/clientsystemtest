package tokenService;


import java.util.UUID;


public class Token {

    private String tokenId;
    
    public Token() {
        this.tokenId = UUID.randomUUID().toString();
    }
    
    public Token(String c) {
    	this.tokenId = c;
    }

	public String getTokenId() {
        return tokenId;
    }
    
	@Override
	public boolean equals(Object other) {
		if (!(other instanceof Token)) {
			return false;
		}
		Token otherToken = (Token) other;
		return tokenId.equals(otherToken.tokenId);
	}
	
	@Override
	public int hashCode() {
		return tokenId.hashCode();
	}
    
}
