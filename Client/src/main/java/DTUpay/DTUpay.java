package DTUpay;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import dtu.ws.fastmoney.BankService;
import userService.DtuPayUserRepresentation;
import paymentService.PaymentRequest;
import tokenService.Token;
import tokenService.TokenRequest;
/**
 * 
 * @author Bijan, Stuart
 *
 */


public class DTUpay {

	String BASE_URL = "http://fastmoney-23.compute.dtu.dk:8084/";
	WebTarget baseUrl;
	TokenRequest tokenRequest;
	PaymentRequest paymentRequest;
	
	
	public DTUpay(BankService bank) {
		Client client = ClientBuilder.newClient();
        baseUrl = client.target(BASE_URL);
	}
	
	public void registerCustomer(String cpr , String firstName , String lastName , String account) {
		DtuPayUserRepresentation customer = createCustomerRepresentation(cpr, firstName, lastName, account);
		registerCustomerREST(customer);
	}
	
	
	public void registerMerchant(String cpr, String firstName, String lastName, String account) {
		DtuPayUserRepresentation merchant = createMerchantRepresentation(cpr, firstName, lastName, account);
		registerMerchantREST(merchant);
	}
	
	
	public List<Token> requestTokens(String cprNumber, int i) {
		createTokenRequestAndSetInformation(cprNumber, i);
		return requestTokensREST();
	}

	
	public Response payWithToken(Token token, String customerAccount, String merchantAccount, BigDecimal amount) {
		createPaymentRequestAndSetInformation(token, customerAccount, merchantAccount, amount);
		return paymentResult();
	}
	
	/*
	 * ---------------------------------------------------------------------------------------------------------------------------------------------
	 */
	
	private DtuPayUserRepresentation createCustomerRepresentation(String cpr, String firstName, String lastName, String account) {
		DtuPayUserRepresentation customer = new DtuPayUserRepresentation();
		customer.setCpr(cpr);
		customer.setFirstName(firstName);
		customer.setLastName(lastName);
		customer.setAccount(account);
		return customer;
	}
	
	private DtuPayUserRepresentation createMerchantRepresentation(String cpr, String firstName, String lastName, String account) {
		DtuPayUserRepresentation merchant = new DtuPayUserRepresentation();
		merchant.setCpr(cpr);
		merchant.setFirstName(firstName);
		merchant.setLastName(lastName);
		merchant.setAccount(account);
		return merchant;
	}
	
	private void registerCustomerREST(DtuPayUserRepresentation customer) {
		baseUrl.path("dtupay").path("customers").request().post(Entity.entity(customer, "application/json"));
	}

	private void registerMerchantREST(DtuPayUserRepresentation merchant) {
		baseUrl.path("dtupay").path("merchants").request().post(Entity.entity(merchant, "application/json"));
	}
	
	private void createTokenRequestAndSetInformation(String cprNumber, int i) {
		tokenRequest = new TokenRequest();
		tokenRequest.setCpr(cprNumber);
		tokenRequest.setNumber(i);
	}
	
	private List<Token> requestTokensREST() {
		Token[] tokens = baseUrl.path("dtupay").path("tokenRequest").request().post(Entity.entity(tokenRequest, "application/json"),Token[].class);
		return Arrays.asList(tokens);
	}
	
	private void createPaymentRequestAndSetInformation(Token token, String customerAccount, String merchantAccount,
			BigDecimal amount) {
		paymentRequest = new PaymentRequest();
		paymentRequest.setAmount(amount);
		paymentRequest.setMerchantAcct(merchantAccount);
		paymentRequest.setCustomerAcct(customerAccount);
		paymentRequest.setToken(token);
	}

	private Response paymentResult() {
		Response result = baseUrl.path("dtupay").path("pay").request().post(Entity.entity(paymentRequest, "application/json"),Response.class);
		
		return result;
	}

}
