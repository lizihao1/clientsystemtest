#Author: your.email@your.domain.com

@tokenPayment
Feature: Payment with token

  @tokenSuccess
  Scenario: successful payment
    Given customer has bank account
    And customer has account in DTUpay
    And customer has at least one unused token
    And merchant has bank account
    When merchant scans the token 
    And requests payment for 100 kroner using the token
    Then the payment succeeds
    And money transfered from the customer account to merchant account in the bank